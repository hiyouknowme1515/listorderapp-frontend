//imported react modules
import React, { Component } from "react";

//imported components or functions
import { isAuthenticated } from "../auth";
import { Redirect , Link} from "react-router-dom";
import { read } from "./apiUser";
import DeleteUser from './DeleteUser'
import { ordersByUser } from "../orders/apiOrder"
import OrdersByUser from "./OrdersByUser"

//Profile class component
class Profile extends Component {
    constructor() {
        super();
        this.state = {
            user: "",
            redirectToSignin: false,
            orders: []
        };
    }
    
    // to fetch user data by its userId
    init = userId => {
        const token = isAuthenticated().token;
        
        read(userId, token).then(data => {
            if (data.error) {
                this.setState({ redirectToSignin: true });
            } else {
                this.setState({ user: data });
                this.loadEvents(data._id);
            }
        });
    };

    //to get userid from URL and call init function
    componentDidMount() {
        const userId = this.props.match.params.userId;
        this.init(userId)
    }

    //it will invoke when component recieve props
    componentWillReceiveProps(props) {
        const userId = props.match.params.userId;
        this.init(userId);
      }
    
    //load events by a particular user
    loadEvents = userId => {
        const token = isAuthenticated().token;
        ordersByUser(userId, token).then(data => {
          if (data.error) {
            console.log(data.error);
          } else {
            this.setState({ orders: data });
          }
        });
      };

    render() {
        const {redirectToSignin, user, orders} = this.state;
        if (redirectToSignin) return <Redirect to="/login" />;

        return (
            <div className="container box box-shadow">
                <h2 className="title is-size-3"><strong>Profile</strong></h2>
                <div className="columns">
                    <div className="column">
                        <div className="is-size-5">Hello <strong>{isAuthenticated().user.name}</strong></div>
                        <div className="is-size-5">Email: <strong>{isAuthenticated().user.email}</strong></div>
                    </div>
                    <div className="column">
                    {isAuthenticated().user &&
                            isAuthenticated().user._id === user._id && (
                                <div className="buttons">
                                    <Link
                                        className="button is-warning is-rounded is-focused mx-2"
                                        to={`/user/edit/${user._id}`}
                                        >
                                        Edit Profile
                                        </Link>
                                   
                                    <DeleteUser userId = { user._id} />
                                </div>
                            )}
                    </div>

                </div>
                
                <OrdersByUser orders={orders} />
            </div>
        );
    }
}
export default Profile;
