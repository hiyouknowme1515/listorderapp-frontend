//Import Some modules
import React, { Component } from "react";
import { Link } from "react-router-dom";

//Events By user Class Component
class OrdersByUser extends Component {
    render() {
        const { orders } = this.props;
        
        return (
            <div className="mt-4">
                    <div className="container">
                        <h3 className="is-size-4 has-text-weight-semibold">{orders.length} Orders</h3>
                        <hr />
                        {orders.map((order, i) => (
                            <div key={i}>
                                <div>
                                    <Link to={`/orders/${order._id}`}>
                                        <div>
                                            <p className="is-size-5">{order.orderName}</p>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        ))}
                    </div>
                
            </div>
        );
    }
}
export default OrdersByUser;