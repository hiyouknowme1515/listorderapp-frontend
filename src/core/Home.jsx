//Required Modules
import React from 'react'

//Imported Images
import AccessDenied from '../media/undraw_access_denied_re_awnf.png'
import Authentication from '../media/undraw_authentication_fsn5.png'
import Security from '../media/undraw_Security_on_re_e491.png'
import Update from '../media/undraw_Portfolio_update_re_jqnp.png'

//Home Functional Component
export default function Home() {
    return (
        <div className="container">
            <div className="has-text-centered is-size-3"><strong>Welcome to the List Order</strong></div>
            <div className="is-size-3 has-text-wieght-bold"><strong>Key Features</strong></div>
            <div className="columns">
                
                <div className="column " >
                    <strong className="is-size-4">Only you can access your listed orders because all the routes are protected</strong>
                </div>
                <div className="column">
                    <img src={`${AccessDenied}`} alt="access denied"style={{height: "60vh"}}></img>
                </div>
            </div>
            <div className="columns">
               
                <div className="column">
                    <div className="is-size-4"><strong>User Login and Register</strong></div>
                </div>
                <div className="column">
                    <img src={`${Authentication}`} style={{height: "60vh"}} alt="User authentication " />
                </div>
                
            </div>
            <div className="columns">
               
                <div className="column">
                    <div className="is-size-4"><strong>User password is stored as hash in database so user's credentials is safe</strong></div>
                </div>
                <div className="column">
                    <img src={`${Security}`} style={{height: "60vh"}} alt="User Security">
                    </img>
                </div>
            </div>
            <div className="columns">
                
                <div className="column">
                    <div className="is-size-4" ><strong>You can update user credentials and order details</strong> </div>
                </div>
                <div className="column">
                    <img src={`${Update}`} style={{height: "60vh"}} alt="Update order and user credentials" />
                </div>
            </div>       

        </div>
    )
}
