//imported some modules
import React from 'react'
import { Route, Switch } from 'react-router-dom'

//Imported Components
import Navbar from './core/Navbar'
import Login from './users/Login'
import Register from './users/Register' 
import Profile from './users/Profile'
import PrivateRoute from './auth/PrivateRoute'
import EditUser from './users/EditUser'
import CreateOrder from './orders/createOrder'
import EditOrder from './orders/EditOrder'
import OrdersByUser from './orders/OrdersByUser'
import SingleOrder from './orders/SingleOrder'
import Home from './core/Home'

//Main Router react function
const MainRouter = () => {
    return (
        <div>
            {/*Navbar */}
            <Navbar />

            <Switch>
                {/*Home page */}
                <Route exact path="/" component={Home} />

                {/* Login  */}
                <Route exact path="/login" component={Login} />

                {/* Register*/}
                <Route exact path="/register" component={Register} />

                {/*Profile component */}
                <PrivateRoute exact path="/user/:userId" component={Profile} />

                {/*Edit User details component */}
                <PrivateRoute exact path = "/user/edit/:userId" component= {EditUser} />

                {/*Create Order component */}
                <PrivateRoute exact path="/order/:userId" component={CreateOrder} />

                {/*Edit Order component */}
                <PrivateRoute
                exact
                path="/order/edit/:orderId"
                component={EditOrder}
                />

                {/*Orders by a specific user component */}
                <PrivateRoute path="/orders/by/:userId" component={OrdersByUser} />

                {/*Single Order component*/}
                <PrivateRoute path="/orders/:orderId" component={SingleOrder} />
            </Switch>

        </div>
    )
}
export default MainRouter