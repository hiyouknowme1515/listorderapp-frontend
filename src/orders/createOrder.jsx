//require some modules
import React, { Component } from "react";
import { Redirect } from "react-router-dom";

//require some components
import { isAuthenticated } from "../auth";
import { create } from "./apiOrder";

//Create Order Class Component
class CreateOrder extends Component {
    constructor() {
        super();
        this.state = {
            orderName: "", 
            orderQuantity: 0,
            orderDate:"",
            orderStatus:"",
            error: "",
            user: {},
            loading: false,
            redirectToProfile: false
        };
    }

    //to set the state of the user when this component loaded
    componentDidMount() {
        this.setState({ user: isAuthenticated().user });
    }

    //Some Client side validation
    isValid = () => {
        const { orderName,  orderQuantity,  orderDate, orderStatus  } = this.state;
        if (orderName.length === 0  || orderQuantity === 0 || orderStatus.length === 0 || orderDate.length === 0) {
            this.setState({ error: "All fields are required", loading: false });
            return false;
        }
        return true;
    };

    //to update state when the input fields value changes
    handleChange = input => event => {
        this.setState({ error: "" });
        const value = event.target.value;  
        this.setState({ [input]: value });
    };

    //to perform creation of event when submit button is clicked
    clickSubmit = event => {
        event.preventDefault();
        this.setState({ loading: true });
        const { orderName,  orderQuantity,  orderDate, orderStatus } = this.state
        this.orderData = {
            orderName,
            orderQuantity,
            orderDate,
            orderStatus
        }

        if (this.isValid()) {
            const userId = isAuthenticated().user._id;
            const token = isAuthenticated().token;
        
            create(userId, token, this.orderData).then(data => {
                if (data.error) this.setState({ error: data.error , loading: false});
                else {
                    this.setState({
                        loading: false,
                        orderName: "", 
                        orderQuantity: 0,
                        orderDate:"",
                        orderStatus:"",
                        redirectToProfile: true
                    });
                }
            });
        }
    };

    //Order form 
    newOrderForm = ({orderName,  orderQuantity,  orderDate, orderStatus } = this.state) => (
        <>
            
            <div className="field">
                <label className="label">Name of Order</label>
                <input
                    onChange={this.handleChange("orderName")}
                    type="text"
                    className="input is-rounded"
                    value={orderName}
                />
            </div>
            <div className="field">
                <label className="label">Order Quantity</label>
                <input
                    onChange={this.handleChange("orderQuantity")}
                    type="number"
                    className="input is-rounded"
                    value={orderQuantity}
                />
            </div>
            <div className="field">
                <label className="label">Date of Placing Order</label>
                <input
                    onChange={this.handleChange("orderDate")}
                    type="date"
                    className="input is-rounded"
                    value={orderDate}
                />
            </div>
            
            <div className="field">
                <label className="label">Order Status</label>
                <input
                    onChange={this.handleChange("orderStatus")}
                    type="text"
                    className="input is-rounded"
                    value={orderStatus}
                />
            </div>

            <button
                onClick={this.clickSubmit}
                className="button is-rounded is-focused is-black"
            >
                Create Order
            </button>
        </>
    );

    render() {
        const {
            orderName,
            orderQuantity,
            orderDate,
            orderStatus,
            error,
            user,
            loading,
            redirectToProfile
        } = this.state;

        if (redirectToProfile) {
            return <Redirect to={`/user/${user._id}`} />;
        }

        return (
            
            <div className="container box box-shadow mt-6">
                <h2 className="title">Create a new Order</h2>
                    <div className="notification is-danger"
                        style={{ display: error ? "" : "none" }}
                        >
                        {error}
                    </div>
                        {loading ?(
                            <progress class="progress is-small is-dark" max="100">15%</progress>
                            ):("")}

                    {this.newOrderForm(orderName,  orderQuantity,  orderDate, orderStatus)}
            </div>
        );
    }
}

export default CreateOrder;
