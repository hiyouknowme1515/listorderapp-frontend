//to create an Order
const create = (userId, token, order) => {
    return fetch(`${process.env.REACT_APP_API_URL}/order/new/${userId}`, {
        method: "POST",
        headers: {
            Accept: "application/json",
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
        },
        body: JSON.stringify(order)

    })
        .then(response => {
            return response.json();
        })
        .catch(err => console.log(err));
};



// to show order event when clicked on read more
const singleOrderfetch = orderId => {
   
    return fetch(`${process.env.REACT_APP_API_URL}/orders/${orderId}`, {
        method: "GET"
    })
        .then(response => {
            return response.json();
        })
        .catch(err => console.log(err));
};

//to delete the order from the data base
const remove = (orderId, token) => {
    return fetch(`${process.env.REACT_APP_API_URL}/order/${orderId}`, {
        method: "DELETE",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`
        }
    })
        .then(response => {
            return response.json();
        })
        .catch(err => console.log(err));
};

//to update the order 
 const update = (orderId, token, order) => { 
    return fetch(`${process.env.REACT_APP_API_URL}/order/${orderId}`, {
        method: "PUT",
        headers: {
            Accept: "application/json",
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
        },
        body: JSON.stringify(order)
    })
        .then(response => {
            return response.json();
        })
        .catch(err => console.log(err));
};

//to get all the  orders a user
 const ordersByUser = (userId, token) => {
    return fetch(`${process.env.REACT_APP_API_URL}/orders/by/${userId}`, {
        method: "GET",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`
        }
    })
        .then(response => {
            return response.json();
        })
        .catch(err => console.log(err));
};

export {
    create,
    singleOrderfetch,
    remove,
    update,
    ordersByUser
}