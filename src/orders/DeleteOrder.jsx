//Imported some modules
import React from 'react'

//Imported some functions
import { remove } from './apiOrder'
import { isAuthenticated } from '../auth'

//Delete Order class component
class DeleteOrder extends React.Component {
    
    //to delete order after confirmation
    delete = () => {
        const orderId = this.props.orderId;
        const token = isAuthenticated().token;
        remove(orderId, token).then(data => {
            if (data.error) {
                console.log(data.error);
            } else {
                this.setState({ redirectToHome: true });
            }
        });
    };
    
    //to ask for confirmation for deletion of orders
    deleteConfirmed = () => {
        let answer = window.confirm('Are you sure you want to delete your order?');
        if (answer) {
            this.delete();
        }
    };

    render(){
        return (
            <button className=" button is-rounded is-danger is-focused " onClick={this.deleteConfirmed}>
                Delete Order
            </button>
        )
    }
}

export default DeleteOrder;