//imported some Modules
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

//imported some components or functions
import { isAuthenticated} from '../auth';
import {  singleOrderfetch} from './apiOrder';
import DeleteOrder  from './DeleteOrder'

//Single order class component
class SingleOrder extends Component{
   constructor()
   {
       super()
       this.state={
           order: ''
       }
   }
    componentDidMount (){
        const orderId = this.props.match.params.orderId;
        singleOrderfetch(orderId).then(data => {
            console.log(data)
            if(data.error){
                console.log(data.error)
            }else{
                this.setState({
                    order: data
                })
            }
        })
    }

    //render a specific order
    renderOrder = order => (
        <div className="card p-3">
            <div className="columns">
                <div className="column">
                    <div className="is-size-6-mobile has-text-wieght-medium is-size-4-desktop">Order Quantity: <strong>{order.orderQuantity}</strong></div>

                    <div className="is-size-4-desktop has-text-wieght-medium is-size-6-mobile">Date of Placing Order: <strong>{new Date(order.orderDate).toDateString()}</strong></div>

                    <div className="is-size-4-desktop has-text-wieght-medium is-size-6-mobile">Order Status: <strong>{order.orderStatus}</strong></div>
                    <div className="is-size-4-desktop has-text-wieght-medium is-size-6-mobile">Updated At: <strong>{new Date(order.updated).toDateString()}</strong></div>
                </div>
                <div className="column">
                    <div className="buttons">
                        <Link to={`/orders/by/${isAuthenticated().user._id}`} className=" button is-rounded is-focused is-dark" >
                            Back to orders
                        </Link>

                        {isAuthenticated().user && isAuthenticated().user._id === order.postedBy._id && (
                            <>
                                <Link to={`/order/edit/${order._id}`} className=" button is-rounded is-focused is-warning">
                                    Update Post
                                </Link>
                                <DeleteOrder orderId = {order._id}/>
                            </>
                        )}
                    
                    </div>
                 </div>
            </div>
            
        </div>
    )

    render(){
        const { order } = this.state;
        return (
            <div className="container box box-shadow mt-6">
                <h2 className="title is-size-2-desktop is-size-4-mobile mt-5 mb-5 has-text-centered"><span className="has-text-wieght-light is-family-code">Order Name: </span> <span className="is-family-secondary"><strong>{order.orderName}</strong></span></h2>
                {!order ? (
                            <progress class="progress is-small is-dark" max="100">15%</progress>
                        ) : (
                    this.renderOrder(order)
                )}
            </div>
        )
    }
}


export default SingleOrder;