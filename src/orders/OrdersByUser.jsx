//Imported some modules
import React from 'react'
import { Link } from 'react-router-dom'

//imported some functions
import { isAuthenticated } from '../auth';
import  DeleteOrder from './DeleteOrder'
import {ordersByUser} from './apiOrder'

//Orders by a specific user class component
class OrdersByUser extends React.Component{
    constructor(){
        super()
        this.state = {
            orders:[]
        }
    }

    componentDidMount(){
        const userId = this.props.match.params.userId;
        const token = isAuthenticated().token;
        ordersByUser(userId,token).then(data => {
            if(data.error ){
                console.log(data.error)
            }
            else{
                this.setState({
                    orders: data.reverse()
                })
            }
        } )
    }

    //render the orders cards collection
    renderOrders = Orders => (
        <div className="columns is-multiline m-2">
            {Orders.map((order, i) => (
                <div className="column is-half card mt-2">
                    <div className="card-content" key={i}>
                        <div className="card-title is-size-5">Order Name: <strong>{order.orderName}</strong></div>
                        <div className="is-size-6">Order Quantity: <strong>{order.orderQuantity}</strong></div>
                        <div className="is-size-6">Date of placing order: <strong>{new Date(order.orderDate).toDateString()}</strong></div>
                        <div className="is-size-6">Order Status: <strong>{order.orderStatus}</strong></div>
                        {isAuthenticated().user && isAuthenticated().user._id === order.postedBy._id && (
                                    <>  
                                        <div className="buttons m-2">
                                            <Link to={`/order/edit/${order._id}`} className=" button is-rounded is-focused is-warning ">
                                                Update Order
                                            </Link>
                                            <DeleteOrder orderId = {order._id}/>
                                            <Link to={`/orders/${order._id}`} className="button is-rounded is-focused is-info">Expand Order</Link>
                                        </div>
                                        
                                    </>
                        )}
                    </div>
                </div>
            ))}
        </div>
    )

    render(){
        const { orders } = this.state
        return (
            <div className="container box box-shadow">
                    {(orders.length===0) ?(
                        <div className="is-size-3"> <strong>No Orders </strong> </div>
                    ):(
                        <div className="is-size-3"> <strong>Recent Orders</strong> </div>
                    )
                }
                {this.renderOrders(orders)}
            </div> 
        )
    }
}

export default OrdersByUser;