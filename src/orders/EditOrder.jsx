//Required some modules
import React, { Component } from "react";
import { Redirect } from "react-router-dom";

//Required some Components
import { singleOrderfetch, update } from "./apiOrder";
import { isAuthenticated } from "../auth";

//Edit Event Component Class
class EditOrder extends Component {
    constructor() {
        super();
        this.state = {
            id: "",
            orderName: "", 
            orderQuantity: 0,
            orderDate:"",
            orderStatus:"",
            redirectToProfile: false,
            error: "",
            loading: false,
            order:{}
        };
    }

    //set the state with use of orderId
    init = orderId => {
        singleOrderfetch(orderId).then(data => {
            if (data.error) {
                this.setState({ redirectToProfile: false });
            } else {
                this.setState({
                    id: data.postedBy._id,
                    orderName: data.orderName,
                    orderQuantity: data.orderQuantity,
                    orderDate: data.orderDate,
                    orderStatus: data.orderStatus,
                    error: "",
                    order: data
                });
            }
        });
    };

    //to set orderId when the component loaded
    componentDidMount() {
        const orderId = this.props.match.params.orderId;
        this.init(orderId);
    }

    //some Client side Validation
    isValid = () => {
        const { orderName,  orderQuantity,  orderDate, orderStatus  } = this.state;
       
        if (orderName.length === 0  || orderQuantity === 0 || orderStatus.length === 0 || orderDate.length === 0) {
            this.setState({ error: "All fields are required", loading: false });
            return false;
        }
        return true;
    };

    //to set the state when input value is changed
    handleChange = input => event => {
        this.setState({ error: "" });
        const value =  event.target.value;
        this.setState({ [input]: value});
    };

    //to perform edit order when submit button is clicked
    clickSubmit = event => {
        event.preventDefault();
        this.setState({ loading: true });
        const { orderName,  orderQuantity,  orderDate, orderStatus } = this.state
        this.orderData = {
            orderName: orderName,
            orderQuantity: orderQuantity,
            orderDate: orderDate,
            orderStatus: orderStatus
        }
        

        if (this.isValid()) {
            const orderId = this.props.match.params.orderId;
            const token = isAuthenticated().token;
            update(orderId, token, this.orderData).then(data => {
                console.log(data)
                if (data.error) this.setState({ error: data.error });
                else {
                    this.setState({
                        loading: false,
                        orderName: "", 
                        orderQuantity: 0,
                        orderDate:"",
                        orderStatus:"",
                        redirectToProfile: true
                    });
                }
            });
        }
    };

    //Edit order Form
    OrderEditForm = (orderName,  orderQuantity,  orderDate, orderStatus ) => (
        <>
            
            <div className="field">
                <label className="label">Name of Order</label>
                <input
                    onChange={this.handleChange("orderName")}
                    type="text"
                    className="input is-rounded"
                    value={orderName}
                />
            </div>
            <div className="field">
                <label className="label">Order Quantity</label>
                <input
                    onChange={this.handleChange("orderQuantity")}
                    type="number"
                    className="input is-rounded"
                    value={orderQuantity}
                />
            </div>
            <div className="field">
                <label className="label">Date of Placing Order</label>
                <input
                    onChange={this.handleChange("orderDate")}
                    type="date"
                    className="input is-rounded"
                    value={orderDate}
                />
            </div>
            
            <div className="field">
                <label className="label">Order Status</label>
                <input
                    onChange={this.handleChange("orderStatus")}
                    type="text"
                    className="input is-rounded"
                    value={orderStatus}
                />
            </div>

            <button
                onClick={this.clickSubmit}
                className="button is-rounded is-focused is-warning"
            >
                Update Order
            </button>
        </>
    );
    render() {
        const {
            id,
            orderName,
            orderQuantity,
            orderDate,
            orderStatus,
            redirectToProfile,
            error,
            loading,
            
        } = this.state;
       
        if (redirectToProfile) {
            return <Redirect to={`/orders/by/${isAuthenticated().user._id}`} />;
        }

        return (
            <div className="container box box-shadow mt-6">
                <div className="is-size-5 mb-2"> Change details of that fields whom you want to update </div>

                <div className="notification is-danger"
                        style={{ display: error ? "" : "none" }}
                        >
                        {error}
                    </div>

                        {loading ?(
                            <progress class="progress is-small is-dark" max="100">15%</progress>
                            ):("")}
                
               
                {isAuthenticated().user._id === id &&
                    this.OrderEditForm(orderName,  orderQuantity,  orderDate, orderStatus)}
            </div>
        );
    }
}

export default EditOrder;
