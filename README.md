# OrderList App Frontend

Technology Used: ReactJS, Bulma

Features

1. User can create his/her account and list their orders
2. They can update and delete their account
3. Update their order, delete their order
4. On User Profile page their orders which they have posted are also shown
5. User can expand each order also
6. Without signup or login then can&#39;t perform any actions like create order, show the orders etc.

Folder structure

+---auth

+---core

+---media

+---orders

\---users

Auth: It contains fuctions used in authentication and authorization of user

Core: It contains core compnents Navbar and Home

Media: It contains different images

Orders: It contains components related to order like OrdersByUser, CreateOrder , DeleteOrder and Update order etc

Users: It contains components realated to user like Login, Register, Profile , DeleteUser, UpdateUser
